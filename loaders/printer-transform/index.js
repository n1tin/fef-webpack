module.exports = function (source) {
    console.log('printer transform!');
    console.log('-------------------------------------------')
    console.log(source, typeof source);
    console.log('-------------------------------------------')

    let newSource = source;
    newSource = newSource.replace(/printer/g, 'console');
    newSource = newSource.replace(/print/g, 'log');

    return newSource;
}