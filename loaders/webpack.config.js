const path = require('path');

module.exports = {
    entry: './src/app.js',
    output: {
        path: path.join(__dirname, 'build'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/, // match the file pattern
                use: [
                    {
                        loader: path.resolve(__dirname, 'printer-transform/index.js'),
                        // loader: 'babel-loader',
                        // options: {}
                    }
                ]
            }
        ]
    }
}