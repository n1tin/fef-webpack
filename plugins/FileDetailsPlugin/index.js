class FileDetails {

    apply(compiler) {
        // console.log(compiler)
        
        compiler.hooks.assetEmitted.tap('File details plugin', (file, contents) => {
            console.log(file);
            console.log('------------------------------------------------------');
            console.log(new Buffer(contents).toString('ascii'));
            console.log('------------------------------------------------------');
            console.log('');
        })
    }

}

module.exports = FileDetails;
