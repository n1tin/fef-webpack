const path = require('path');
const FileDetailsPlugin = require('./FileDetailsPlugin')

module.exports = {
    entry: './src/app.js',
    output: {
        path: path.join(__dirname, 'build'),
        filename: 'bundle.js'
    },
    plugins: [
        new FileDetailsPlugin()
    ]
}